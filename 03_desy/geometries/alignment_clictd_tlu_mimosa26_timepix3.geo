[TLU_0]
orientation = 0,0,0
orientation_mode = "xyz"
position = 0,0,0
role = "auxiliary"
time_resolution = 1s
type = "tlu"

[MIMOSA26_0]
mask_file = "mask_MIMOSA26_0.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = -3.67478deg,-1.41841deg,-0.431953deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = 355.205um,203.113um,0
spatial_resolution = 4um,4um
time_resolution = 230us
type = "mimosa26"

[MIMOSA26_1]
mask_file = "mask_MIMOSA26_1.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = 0.569119deg,-0.572786deg,-0.742725deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = -1.14759mm,189.445um,277mm
spatial_resolution = 4um,4um
time_resolution = 230us
type = "mimosa26"

[MIMOSA26_2]
mask_file = "mask_MIMOSA26_2.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = -0.155272deg,0.064515deg,0.0013751deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = -1.11946mm,95.476um,305mm
role = "reference"
spatial_resolution = 4um,4um
time_resolution = 230us
type = "mimosa26"

[CLICTD_0]
mask_file = "mask_CLICTD_run3390.txt"
material_budget = 0.015
number_of_pixels = 128, 128
orientation = 177.882deg,-0.778478deg,89.2679deg
orientation_mode = "xyz"
pixel_pitch = 37.5um,30um
position = -237.812um,207.526um,344mm
role = "dut"
spatial_resolution = 10.8um,8.7um
time_resolution = 10ns
type = "clictd"

[MIMOSA26_3]
mask_file = "mask_MIMOSA26_3.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = 2.59452deg,0.757393deg,-0.263732deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = -665.274um,-19.253um,364mm
spatial_resolution = 4um,4um
time_resolution = 230us
type = "mimosa26"

[MIMOSA26_4]
mask_file = "mask_MIMOSA26_4.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = 2.89246deg,0.229355deg,-0.0818757deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = -496.624um,109.98um,391mm
spatial_resolution = 4um,4um
time_resolution = 230us
type = "mimosa26"

[MIMOSA26_5]
mask_file = "mask_MIMOSA26_5.txt"
material_budget = 0.00075
number_of_pixels = 1152, 576
orientation = -5.42734deg,-1.69034deg,-0.235199deg
orientation_mode = "xyz"
pixel_pitch = 18.4um,18.4um
position = -292.45um,-475.25um,640mm
spatial_resolution = 4um,4um
time_resolution = 230us
type = "mimosa26"

[Timepix3_0]
material_budget = 0.00075
number_of_pixels = 256, 256
orientation = 173.512deg,-1.21719deg,-0.235658deg
orientation_mode = "xyz"
pixel_pitch = 55um,55um
position = -490.084um,-1.43618mm,685mm
spatial_resolution = 10um,10um
time_offset = 260ns
time_resolution = 1.56ns
type = "timepix3"

